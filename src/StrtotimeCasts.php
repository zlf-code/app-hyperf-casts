<?php
declare(strict_types=1);

namespace Zlf\AppHyperfCasts;

use Hyperf\Contract\CastsAttributes;

/**
 * 数据库时间写入检查
 * Class StrtotimeCasts
 * @package Core\Casts
 */
class StrtotimeCasts implements CastsAttributes
{
    /**
     * 取出时间时转换
     */
    public function get($model, $key, $value, $attributes): int
    {
        return (int)$value;
    }

    /**
     * 储存时间时转换
     */
    public function set($model, $key, $value, $attributes): int
    {
        $value = (string)$value;
        if (is_numeric($value)) return (int)$value;
        return (int)strtotime($value);
    }
}
