<?php

declare(strict_types=1);

namespace Zlf\AppHyperfCasts;

use Hyperf\Contract\CastsAttributes;

/**
 * html转义
 * Class ArrjoinCasts
 * @package Core\Casts
 */
class HtmlEscapeCasts implements CastsAttributes
{
    /**
     * 获取结果
     */
    public function get($model, string $key, $value, array $attributes): string
    {
        return is_string($value) ? htmlspecialchars_decode($value) : '';
    }

    /**
     * 设置数据
     */
    public function set($model, string $key, $value, array $attributes): string
    {
        return is_string($value) ? htmlspecialchars($value) : '';
    }
}
