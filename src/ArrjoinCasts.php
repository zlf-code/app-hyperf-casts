<?php
declare(strict_types=1);

namespace Zlf\AppHyperfCasts;

use Zlf\Unit\Str;
use Hyperf\Contract\CastsAttributes;


/**
 * 数组连接转字符串 如 ['a','b']  a,b
 * Class ArrjoinCasts
 * @package Core\Casts
 */
class ArrjoinCasts implements CastsAttributes
{
    /**
     * 获取结果
     */
    public function get($model, $key, $value, $attributes)
    {
        if (is_array($value)) {
            return $value;
        } elseif (is_string($value) && strlen($value) > 0) {
            return Str::explode(',', $value);
        }
        return [];
    }

    /**
     * 设置数据
     */
    public function set($model, $key, $value, $attributes)
    {
        if (is_array($value)) {
            return  implode(',',$value);
        } elseif (is_string($value) && strlen($value)>0) {
            return $value;
        }
        return '';
    }
}
