<?php
declare(strict_types=1);

namespace Zlf\AppHyperfCasts;

use Hyperf\Contract\CastsAttributes;


/**
 * 金额转换，保留2位小数
 * Class ArrjoinCasts
 * @package Core\Casts
 */
class DecimalCasts implements CastsAttributes
{
    /**
     * 获取结果
     */
    public function get($model, $key, $value, $attributes): float
    {
        return floatval($value ? $value : '0');
    }

    /**
     * 设置数据
     */
    public function set($model, $key, $value, $attributes): float|int
    {
        if (empty($value)) {
            return 0;
        }
        if (is_numeric($value)) {
            return round(floatval($value), 2);
        }
        return 0;
    }
}
