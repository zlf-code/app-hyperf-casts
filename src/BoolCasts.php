<?php

declare(strict_types=1);

namespace Zlf\AppHyperfCasts;

use Hyperf\Contract\CastsAttributes;


/**
 * 布尔值转换
 * Class ArrjoinCasts
 * @package Core\Casts
 */
class BoolCasts implements CastsAttributes
{
    /**
     * 获取结果
     */
    public function get($model, string $key, $value, array $attributes): bool
    {
        if ($value === 1 || $value === '1' || $value === true) {
            return true;
        }
        return false;
    }

    /**
     * 设置数据
     */
    public function set($model, string $key, $value, array $attributes): int
    {
        if ($value === 1 || $value === '1' || $value === true) {
            return 1;
        }
        return 0;
    }
}
