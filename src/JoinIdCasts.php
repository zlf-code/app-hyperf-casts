<?php
declare(strict_types=1);

namespace Zlf\AppHyperfCasts;

use Hyperf\Contract\CastsAttributes;

/**
 * int类型观连转换器
 * Class StrtotimeCasts
 * @package Core\Casts
 */
class JoinIdCasts implements CastsAttributes
{
    /**
     * 取出分类信息,未设置或设置的0时返回空字符串代表没设置
     */
    public function get($model, $key, $value, $attributes): int|string
    {
        $value = (int)$value;
        return $value > 0 ? $value : '';
    }

    /**
     * 把分类数据转int
     */
    public function set($model, $key, $value, $attributes): int
    {
        return (int)$value;
    }
}
